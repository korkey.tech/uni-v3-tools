import { Controller } from "@hotwired/stimulus"

import pairTemplate from 'string:./partials/pair.pug'
import { render } from 'pug'

export default class extends Controller {
  drawPairs(positionsByPair) {
    for( const [pairAddress,positions] of Object.entries(positionsByPair) ) {

      this.element.insertAdjacentHTML('beforeend', render(
        pairTemplate, { pairAddress: pairAddress }
      ))

      Promise.resolve().then(() => {
        this.pairController(
          this.element.querySelector(
            `.pair[data-pair-address-value="${pairAddress}"]`
          )
        ).drawPair(positions)

      })
    }
  }


  pairController(element) {
    return this.application.getControllerForElementAndIdentifier(element, 'pair');
  }

}
