import { Controller } from "@hotwired/stimulus"
import { isAddress } from '@ethersproject/address'
import { render } from 'pug'

export default class extends Controller {
  static targets = [
    'list'
  ]

  localStorageKey = 'positions-history'

  connect() {
    this.writeHistoryList()
  }

  toggle(event) {
    document.body.classList.toggle('history-toggled')
  }

  addHistory(rawHistoryItem) {
    const newHistoryItem = {
      addresses: rawHistoryItem.filter(item => isAddress(item)),
      datetime: new Date()
    }

    // If this historyItem is here, remove it
    const historyArray = (this.history || []).filter(item => {
      return !this.arraysEqual(item.addresses, newHistoryItem.addresses)
    })

    historyArray.unshift(newHistoryItem)
    this.writeHistory(historyArray)
  }

  writeHistory(newHistory) {
    localStorage.setItem(this.localStorageKey, JSON.stringify(newHistory.slice(0,20)))
    this.writeHistoryList()
  }

  writeHistoryList() {
    this.listTarget.innerHTML = ''

    if(this.history.length == 0) {
      this.listTarget.innerHTML = render('div ~ No History Yet ~')
    } else {
      this.history.forEach(item => {
        this.listTarget.insertAdjacentHTML('beforeend', this.historyListItem(item))
      })
    }
  }

  historyListItem(listItem) {
    let historyDate = new Date(listItem.datetime)
    let url = window.location.origin + window.location.pathname + '#' + listItem.addresses.join(',')

    return render(`li
      a(data-action="history#toggle" href="${url}" class="history-item")
        | ${listItem.addresses.join(',')}
      time(datetime="${historyDate.toJSON()}") ${historyDate.toLocaleString()}`
    )
  }

  get history() {
    return JSON.parse(localStorage.getItem(this.localStorageKey)) || []
  }


  arraysEqual(a,b) {
    a = Array.isArray(a) ? a : [];
    b = Array.isArray(b) ? b : [];
    return a.length === b.length && a.every((el, ix) => el === b[ix]);
  }
}

