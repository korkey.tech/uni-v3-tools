import { Controller } from "@hotwired/stimulus"
import { isAddress } from '@ethersproject/address'
import positionsQuery from "./graphql/positions-query.graphql"
import { DEXES } from "../constants"
import { Token } from '@pancakeswap/swap-sdk-core'
import { Pool } from '../liquidity/helpers/9mm.mjs'

export default class extends Controller {
  static targets = [
    'input'
  ]

  connect() {
    this.inputTarget.addEventListener('paste', event => this.cleanInputData(event))

    window.addEventListener('load', () => {
      this.loadQueryFromHash()
    })

    window.addEventListener("popstate", () => {
      this.loadQueryFromHash()
    })
  }

  loadQueryFromHash() {
    const rawNavigatedAddresses = window.location.hash.replace(/^\#/, ''),
          navigatedAddresses = this.filterAddresses(rawNavigatedAddresses)

    if(navigatedAddresses.length > 0) {
      this.inputTarget.textContent = navigatedAddresses.join("\n")
      this.query(navigatedAddresses)
    } else {
      this.inputTarget.textContent = null
      if(this.ownersController) {
        this.ownersController.clearOwners()
      }
    }
  }

  cleanInputData(event) {
    event.preventDefault();
    const text = event.clipboardData.getData('text/plain');

    document.execCommand("insertHTML", false, text);

    this.inputTarget.textContent = this.filterAddresses(this.inputTarget.textContent + "\n" + text).join("\n")

    const selection = window.getSelection()
    selection.selectAllChildren(this.inputTarget)
    selection.collapseToEnd()
  }

  maybeQuery(event) {
    const input = this.filterAddresses(event.target.textContent)

    if(input.length > 0) {

      event.target.classList.remove('incorrect-format')

      window.history.pushState({query: input}, "", `#${input}`);

      // Convert all seperator chars to newlines
      this.inputTarget.textContent =
        event.target.textContent.replace(this.splitRegex, "\n")

      this.query(input)

    } else if(input == '') {

      event.target.classList.remove('incorrect-format')

      window.history.pushState({query: input}, "", `#${input}`);

      this.ownersController.clearOwners()

    } else {

      event.target.classList.add('incorrect-format')

    }
  }

  query(addresses) {
    if(this.ownersController) {
      this.ownersController.clearOwners()
    }

    if(this.element.querySelector('.query-summary')) {
      this.element.querySelector('.query-summary').remove()
    }

    fetch(DEXES[window.selectedDex], {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        query: positionsQuery,
        variables: {
          addresses: addresses
        }
      })
    })
    .then(res => res.json())
    .then(result => {
      this.historyController.addHistory(addresses)
      this.processData(result.data.positions)
    })
  }

  processData(positions) {
    if(positions.length == 0) {
      this.ownersController.element.innerHTML = `
        <h3 style="text-align: center;">No Positions</h3>
      `

      return;
    }

    try {

      const positionsByOwner = {}
      let totalPositions = 0, inRangePositions = 0,
          outRangePositions = 0, closedPositions = 0

      positions.forEach(rawPosition => {

        // TODO Maybe implement v2 in the future?
        if(rawPosition.pool.tick === null) {
          // This is a V2 position.
          return;
        }

        try {

          let token0 = new Token(
            1,
            rawPosition.pool.token0.address,
            parseInt(rawPosition.pool.token0.decimals),
            rawPosition.pool.token0.symbol,
            rawPosition.pool.token0.name
          )

          let token1 = new Token(
            1,
            rawPosition.pool.token1.address,
            parseInt(rawPosition.pool.token1.decimals),
            rawPosition.pool.token1.symbol,
            rawPosition.pool.token1.name
          )

          let pool = new Pool(
            token0,
            token1,
            parseInt(rawPosition.pool.feeTier),
            parseInt(rawPosition.pool.sqrtPrice),
            parseInt(rawPosition.pool.liquidity),
            parseInt(rawPosition.pool.tick)
          )

          let thisPosition = {
            pool: pool,
            tickLower: Number(rawPosition.tickLower.tickIdx),
            tickUpper: Number(rawPosition.tickUpper.tickIdx),
            liquidity: rawPosition.liquidity,
            depositedToken0: parseFloat(rawPosition.depositedToken0),
            depositedToken1: parseFloat(rawPosition.depositedToken1)
          }

          positionsByOwner[rawPosition.owner] ??= {}
          positionsByOwner[rawPosition.owner][rawPosition.pool.id] ??= {}
          positionsByOwner[rawPosition.owner][rawPosition.pool.id][rawPosition.id] = thisPosition

          totalPositions++

          if(thisPosition.liquidity != 0) {
            if(pool.tickCurrent > thisPosition.tickLower &&
               pool.tickCurrent < thisPosition.tickUpper) {
              inRangePositions++
            } else {
              outRangePositions++
            }
          } else {
            closedPositions++
          }
        } catch(e) {
          console.error(e)
        }
      })

      // Print a summary
      this.element.insertAdjacentHTML('afterbegin', `
        <h2 class="query-summary" style="text-align: center;">
          Found ${Object.keys(positionsByOwner).length} Wallets with ${totalPositions} Positions
          <br>
          ${inRangePositions} In Range, ${outRangePositions} Out of Range & ${closedPositions} Closed
        </h2>
      `)

      this.ownersController.outputPositionsByOwner(positionsByOwner)

    } catch (error) {

      console.error(error)

      this.ownersController.element.innerHTML = `
        <h3 style="text-align: center;color: var(--error-color);">Something messed up. Whoops.</h3>
      `

      return;
    }
  }

  filterAddresses(string) {
    return [...new Set(string.split(this.splitRegex).filter(addy => {
      return isAddress(addy)
    }))]
  }

  get splitRegex() {
    return /[,;:\/\\\|\r\n\t]+/
  }

  get ownersController() {
    return this.application.getControllerForElementAndIdentifier(document.querySelector('.owners'), 'owners');
  }

  get historyController() {
    return this.application.getControllerForElementAndIdentifier(document.querySelector('.history'), 'history');
  }
}

