import { Controller } from "@hotwired/stimulus"
import { Position } from '@pancakeswap/v3-sdk'
import { POSITION_URLS } from '../constants'

export default class extends Controller {
  static targets = [
    'id',
    'inRange',
    'price',
    'token0Liquidity',
    'token1Liquidity'
  ]

  static values = {
    id: Number
  }

  drawPosition(rawPosition) {
    const positionUrl = POSITION_URLS[window.selectedDex].replace('{poolId}', this.idValue)

    this.idTarget.innerHTML =
      `<a target="_blank" href="${positionUrl}">${this.idValue}</a>`

    const position = new Position({
      pool: rawPosition.pool,
      tickLower: rawPosition.tickLower,
      tickUpper: rawPosition.tickUpper,
      liquidity: rawPosition.liquidity
    })

    let lower = position.token0PriceLower,
        upper = position.token0PriceUpper,
        current0 = position.pool.token0Price,
        current1 = position.pool.token1Price,
        amount0 = null,
        amount1 = null

    this.priceTarget.innerHTML = `
      <div class="token-price">
        <small>${position.pool.token1.symbol} per ${position.pool.token0.symbol} Range</small>
        <span class="min-price">${lower.toFixed(10)}</span>
        <span class="sep">⟷</span>
        <span class="max-price">${upper.toFixed(10)}</span>
      </div>
      <div class="token-price">
        <span class="min-price">${upper.invert().toFixed(10)}</span>
        <span class="sep">⟷</span>
        <span class="max-price">${lower.invert().toFixed(10)}</span>
        <small>${position.pool.token0.symbol} per ${position.pool.token1.symbol} Range</small>
      </div>
    `

    if(position.liquidity == 0) {
      this.element.classList.add('closed')
      this.inRangeTarget.classList.add('closed')

      amount0 =
        `<span>${rawPosition.depositedToken0.toFixed(2)}</span>`
      amount1 =
        `<span>${rawPosition.depositedToken1.toFixed(2)}</span>`

    } else {
      amount0 =
        `<span>${position.amount0.toSignificant(4)} (${this.getRatio(lower, current0, upper)}%)</span>`
      amount1 =
        `<span>${position.amount1.toSignificant(4)} (${this.getRatio(upper.invert(), current1, lower.invert())}%)</span>`
    }

    if(this.inRange(position)) {
      this.inRangeTarget.classList.add('in-range')
    }

    this.token0LiquidityTarget.innerHTML = `
      <small>Pooled ${position.pool.token0.symbol}</small>
      ${amount0}
    `

    this.token1LiquidityTarget.innerHTML = `
      ${amount1}
      <small>Pooled ${position.pool.token1.symbol}</small>
    `
  }

  inRange(position) {
    const currentTick = position.pool.tickCurrent

    return currentTick > position.tickLower && currentTick < position.tickUpper
  }

  getRatio(lower, current, upper) {
    try {
      if (!current.greaterThan(lower)) {
        return 100
      } else if (!current.lessThan(upper)) {
        return 0
      }

      const a = Number.parseFloat(lower.toSignificant(15))
      const b = Number.parseFloat(upper.toSignificant(15))
      const c = Number.parseFloat(current.toSignificant(15))

      const ratio = Math.floor((1 / ((Math.sqrt(a * b) - Math.sqrt(b * c)) / (c - Math.sqrt(b * c)) + 1)) * 100)

      if (ratio < 0 || ratio > 100) {
        throw Error('Out of range')
      }

      return ratio
    } catch {
      return undefined
    }
  }
}

