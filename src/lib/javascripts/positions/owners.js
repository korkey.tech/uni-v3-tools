import { Controller } from "@hotwired/stimulus"
import { EXPLORER_URLS } from '../constants'
import ownerTemplate from 'string:./partials/owner.pug'
import { render } from 'pug'

export default class extends Controller {
  static targets = [
    'positions'
  ]

  static values = {
    address: String
  }

  clearOwners() {
    this.element.innerHTML = '';
  }

  outputPositionsByOwner(positionsByOwner) {
    for( const [ownerAddress,positionsByPair] of Object.entries(positionsByOwner) ) {

      this.element.insertAdjacentHTML('beforeend', render(
        ownerTemplate, {
          url: EXPLORER_URLS[window.selectedDex].replace('{addy}', ownerAddress),
          ownerAdress: ownerAddress
        }
      ))

      Promise.resolve().then(() => {
        this.positionsController(
          this.element.querySelector(
            `.positions[data-owners-address="${ownerAddress}"]`
          )
        ).drawPairs(positionsByPair)

      })
    }
  }


  positionsController(element) {
    return this.application.getControllerForElementAndIdentifier(element, 'positions');
  }

}
