import { Controller } from "@hotwired/stimulus"
import { getAddress } from '@ethersproject/address'
import { ICON_URLS } from '../constants'
import positionTemplate from 'string:./partials/position.pug'
import { render } from 'pug'

export default class extends Controller {
  static targets = [
    'tickerLabel',
    'tickerPrice',
    'fee',
    'pairPositions'
  ]

  static values = {
    address: String
  }

  drawPair(positions) {

    // Get uniq values from all pools. Then get the first.
    // Was gonna write a validation for multi pools, but whatever
    const pool = [... new Set(Object.values(positions).map(position => {
      return position.pool
    }))][0]

    const token0 = pool.token0,
          token1 = pool.token1

    this.drawTickers(pool, token0, token1)

    this.feeTarget.textContent = `${pool.fee / 10 ** 4}%`

    for( const [positionId,position] of Object.entries(positions) ) {
      this.pairPositionsTarget.insertAdjacentHTML('beforeend', render(
        positionTemplate, { positionId: positionId }
      ))

      Promise.resolve().then(() => {

        this.positionController(
          this.pairPositionsTarget.querySelector(
            `.position[data-position-id-value="${positionId}"]`
          )
        ).drawPosition(position)

      })
    }
  }

  drawTickers(pool, token0, token1) {
    this.tickerLabelTarget.innerHTML = `
      <div class="token-icon">
        <object data="${this.tokenIconUrl(token0)}" type="image/png"></object>
        <object data="${this.tokenIconUrl(token1)}" type="image/png"></object>
      </div>
      <label>
        <a target="_blank" href="/liquidity/${window.selectedDex}#${this.addressValue}">
          ${token0.symbol}
          <span class="sep">/</span>
          ${token1.symbol}
        </a>
      </label>
    `

    this.tickerPriceTarget.innerHTML = `
      <span class="price">
        ${pool.priceOf(token0).toFixed(10)} <small>${token1.symbol} per ${token0.symbol}</small>
        <span class="sep"> — </span>
        ${pool.priceOf(token1).toFixed(10)} <small>${token0.symbol} per ${token1.symbol}</small>
      </span>
    `
  }

  positionController(element) {
    return this.application.getControllerForElementAndIdentifier(element, 'position');
  }

  tokenIconUrl(token) {
    return ICON_URLS[window.selectedDex]
           .replace('{addy}', token.address)
           .replace('{addyVer}', getAddress(token.address))
           .replace('{sym}', token.symbol.toLowerCase())
  }
}

