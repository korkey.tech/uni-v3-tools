import { Controller } from "@hotwired/stimulus"
import { BigNumber, utils } from 'ethers'
import { displayPool, displayPositions } from './helpers/display'
import { DEXES } from '../constants'
import Pool from './helpers/pool'

import tokenQuery from "./graphql/token.graphql"
import poolResultsQuery from "./graphql/pool-results.graphql"

export default class extends Controller {
  static targets = [
    'input',
    'poolResults'
  ]

  queryTimer = null

  connect() {
    this.inputTarget.addEventListener('paste', event => this.cleanInputData(event))

    window.addEventListener('load', () => {
      this.loadPoolFromHash()
    })

    window.addEventListener("popstate", () => {
      this.loadPoolFromHash()
    })
  }

  loadPoolFromHash() {
    const urlHash = window.location.hash.replace(/^\#/, '').split(':')

    if(urlHash.length == 2) {
      if(utils.isAddress(urlHash[0])) {
        displayPool(urlHash[0])
        displayPositions(urlHash[0], urlHash[1])
      }
    } else if(utils.isAddress(urlHash[0])) {
      displayPool(urlHash[0])
    }
  }

  cleanInputData(event) {
    event.preventDefault();
    const text = event.clipboardData.getData('text/plain');
    document.execCommand("insertHTML", false, text);
  }

  showResults() {
    this.poolResultsTarget.classList.remove('hidden')
  }

  hideResults() {
    this.poolResultsTarget.classList.add('hidden')
  }

  maybeQuery(event) {
    clearTimeout(this.queryTimer)

    const queryString = event.target.textContent.trim()

    if(queryString.length > 1) {

      this.queryTimer = setTimeout(() => {
        this.query(queryString)
      }, 500)

    } else {

      this.poolResultsController.clear()

    }
  }

  async query(queryString) {
    this.poolResultsController.showLoading()

    const tokenResults = await this.getTokenResults(queryString),
          poolResults = await this.getPoolResults(queryString, tokenResults)

    this.poolResultsController.clear()
    this.poolResultsController.drawPoolResults(poolResults)
  }


  async getTokenResults(queryString) {
    let _tokenQuery = tokenQuery

    if(window.selectedDex.startsWith('9inch')) {
      _tokenQuery = _tokenQuery.replaceAll('derivedETH', 'derivedPLS')
    }

    const response = await fetch(DEXES[window.selectedDex], {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        query: _tokenQuery,
        variables: {
          symbol: queryString.toUpperCase(),
          address: queryString.toLowerCase()
        }
      })
    })

    if(!response.ok) {
      return false
    }

    const result = await response.json()
    let parsedResult = []

    parsedResult.push( ...result.data.asSymbol )
    parsedResult.push( ...result.data.asAddress )

    parsedResult.sort((a,b) => {
      try { return new Pool(b).tvlUSD - new Pool(a).tvlUSD }
      catch(e) { console.error(e); return 0; }
    })

    return this.collectTokenIds(parsedResult)
  }

  async getPoolResults(queryString, tokens) {
    let _poolResultsQuery = poolResultsQuery

    if(window.selectedDex.startsWith('9inch')) {
      _poolResultsQuery = _poolResultsQuery.replaceAll('derivedETH', 'derivedPLS')
    }

    const response = await fetch(DEXES[window.selectedDex], {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        query: _poolResultsQuery,
        variables: {
          tokens: tokens,
          id: queryString.toLowerCase()
        }
      })
    })

    if(!response.ok) {
      return false
    }

    const result = await response.json()
    let parsedResult = []

    parsedResult.push( ...result.data.as0 )
    parsedResult.push( ...result.data.as1 )
    parsedResult.push( ...result.data.asAddress )

    parsedResult.sort((a,b) => {
      try { return new Pool(b).tvlUSD - new Pool(a).tvlUSD }
      catch(e) { console.error(e); return 0; }
    })

    return this.collectPools(parsedResult)
  }

  collectTokenIds(tokens) {
    return tokens.filter(token => {
      return token.volume !== '0'
    }).map(token => {
      return token.id
    })
  }

  collectPools(pools) {
    return pools.filter(pool => {
      return pool.volumeToken0 !== '0' || pool.volumeToken1 !== '0'
    })
  }

  get poolResultsController() {
    return this.application.getControllerForElementAndIdentifier(document.querySelector('#pool-results'), 'pool-results');
  }

}

