import { Controller } from "@hotwired/stimulus"
import Pool from './helpers/pool'
import keyBy from './helpers/keyby'
import { DEXES, POSITION_URLS } from '../constants'

import { Position, tickToPrice } from './helpers/9mm.mjs'

import positionsQuery from "./graphql/positions.graphql"
import tickQuery from "./graphql/tick.graphql"

import { utils } from 'ethers'

import { render } from 'pug'
import positionsTemplate from 'string:./partials/positions.pug'

export default class extends Controller {
  static values = {
    poolAddress: String,
    tick: Number
  }

  positions = []
  token0Price = null
  token1Price = null

  async connect() {
    // Draw pool info and chart. We have the pool address
    // Set a interval to query the pool again, redraw if changed

    if(this.hasPoolAddressValue && this.hasTickValue) {
      this.positions = await this.fetchPositions()

      this.drawPositions()
    }
  }

  drawPositions() {
    const poolClass = this.poolController.pool,
          token0 = poolClass.token0,
          token1 = poolClass.token1

    let positionsByToken = {},
        lockedByToken = {}

    this.token0Price = poolClass.currencyFormat
    .format(
      tickToPrice(
        token0,
        token1,
        this.tickValue
      ).toFixed(20)
    )

    this.token1Price = poolClass.currencyFormat
    .format(
      tickToPrice(
        token1,
        token0,
        this.tickValue
      ).toFixed(20)
    )

    positionsByToken[token0.address] = {}
    positionsByToken[token1.address] = {}

    lockedByToken[token0.address] = 0
    lockedByToken[token1.address] = 0

    this.positions.forEach(position => {
      let tickRange = parseInt((
            Math.abs(position.tickLower.tickIdx) - Math.abs(position.tickUpper.tickIdx)
          ) / poolClass.tickSpacing)

      if(tickRange < 0) {
        tickRange = parseInt((
          Math.abs(position.tickUpper.tickIdx) - Math.abs(position.tickLower.tickIdx)
        ) / poolClass.tickSpacing)
      }

      let lockedToken0 = position.depositedToken0 / tickRange,
          lockedToken1 = position.depositedToken1 / tickRange

      if(tickRange === 0) {
        lockedToken0 = parseInt(position.depositedToken0)
        lockedToken1 = parseInt(position.depositedToken1)
      }

      if(lockedToken0 > 0) {
        positionsByToken[token0.address][position.owner] ??= []

        positionsByToken[token0.address][position.owner].push({
          ...position,
          tickRange: tickRange,
          lockedTokens: lockedToken0,
          allLockedTokens: position.depositedToken0
        })

        lockedByToken[token0.address] += lockedToken0
      }

      if(lockedToken1 > 0) {
        positionsByToken[token1.address][position.owner] ??= []

        positionsByToken[token1.address][position.owner].push({
          ...position,
          tickRange: tickRange,
          lockedTokens: lockedToken1,
          allLockedTokens: position.depositedToken1
        })

        lockedByToken[token1.address] += lockedToken1
      }
    })

    this.element.insertAdjacentHTML('afterbegin', render(
      `.tick-details
        h3 Tick: ${this.tickValue}
        label Total ${token0.symbol} Locked: ${poolClass.condensedNumberFormat.format(lockedByToken[token0.address])}
        label Total ${token1.symbol} Locked: ${poolClass.condensedNumberFormat.format(lockedByToken[token1.address])}
        label ${token0.symbol}/${token1.symbol}: ${this.token0Price}
        label ${token1.symbol}/${token0.symbol}: ${this.token1Price}`
    ))

    for (const [tokenAddress, positionsByOwner] of Object.entries(positionsByToken)) {

      let token = tokenAddress == token0.address ? token0 : token1,
          sortedOwners = Object.fromEntries(Object.entries(positionsByOwner).sort((a, b) => {
            let aPos = a.at(-1),
                bPos = b.at(-1),
                aTot = aPos.map(pos => pos.lockedTokens).reduce((a, b) => a + b, 0),
                bTot = bPos.map(pos => pos.lockedTokens).reduce((a, b) => a + b, 0)

            return aTot - bTot
          }))

      this.element.insertAdjacentHTML('beforeend', render(
        `.token-positions(data-token="${token.address}")
          label ${token.name} (${token.symbol})
          .owner-positions`
      ))

      for (const [ownerAddress, positions] of Object.entries(sortedOwners)) {

        this.element.querySelector(
          `.token-positions[data-token="${token.address}"] .owner-positions`
        ).insertAdjacentHTML('afterbegin', render(positionsTemplate, {
          dex: window.selectedDex,
          ownerAddress: ownerAddress,
          token0: token,
          token1: token.address == token0.address ? token1 : token0,
          positions: positions.map(position => {
            let positionKlass = new Position({
              pool: this.poolController.pool,
              tickLower: Number(position.tickLower.tickIdx),
              tickUpper: Number(position.tickUpper.tickIdx),
              liquidity: position.liquidity
            })

            if(tokenAddress == token0.address) {
              position.lower = poolClass.condensedNumberFormat.format(
                positionKlass.token0PriceLower.toFixed(20)
              )

              position.upper = poolClass.condensedNumberFormat.format(
                positionKlass.token0PriceUpper.toFixed(20)
              )
            } else if(tokenAddress == token1.address) {
              position.lower = poolClass.condensedNumberFormat.format(
                positionKlass.token0PriceLower.invert().toFixed(20)
              )

              position.upper = poolClass.condensedNumberFormat.format(
                positionKlass.token0PriceUpper.invert().toFixed(20)
              )
            }

            if(position.lower == 0 || position.upper == 0) {
              position.isFullRange = true
            } else {
              position.isFullRange = false
            }

            position.lockedTokens = poolClass.condensedNumberFormat.format(position.lockedTokens)
            position.allLockedTokens = poolClass.condensedNumberFormat.format(position.allLockedTokens)

            position.url = POSITION_URLS[window.selectedDex].replace('{poolId}', position.id)

            return position
          })
        }))
      }
    }

    this.element.scrollTo()
  }

  async fetchPositions() {
    let positions = [], allPositions = [], skip = 0

    do {
      const response = await fetch(DEXES[window.selectedDex], {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          query: positionsQuery,
          variables: {
            pool: this.poolAddressValue.toLowerCase(),
            skip: skip
          }
        })
      })

      if(!response.ok) {
        return false
      }

      const json = await response.json()

      positions = json.data.positions
      allPositions = allPositions.concat(positions)
      skip += 1000

    } while (positions.length > 0)

    return allPositions.filter(position => {
      return parseInt(position.tickLower.tickIdx) <= this.tickValue &&
      parseInt(position.tickUpper.tickIdx) > this.tickValue
    })
  }

  get positionsTemplate() {
    return fs.readFileSync(__dirname + '/../../partials/positions.pug', 'utf8');
  }

  get poolController() {
    return this.application.getControllerForElementAndIdentifier(document.querySelector('#pool'), 'pool');
  }
}

