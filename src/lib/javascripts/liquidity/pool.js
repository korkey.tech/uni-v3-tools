import { Controller } from "@hotwired/stimulus"
import LiquidityChart from './helpers/liquidity-chart'
import Pool from './helpers/pool'
import { DEXES } from '../constants'

import poolQuery from "./graphql/pool.graphql"

import { utils } from 'ethers'

import { render } from 'pug'
import poolTemplate from 'string:./partials/pool.pug'

export default class extends Controller {
  static values = {
    address: String
  }

  static targets = [
    'info',
    'liquidityChart'
  ]

  rawPool = null
  inverted = false

  async connect() {
    // Draw pool info and chart. We have the pool address
    // Set a interval to query the pool again, redraw if changed
    if(this.hasAddressValue) {
      let thisRawPool = await this.poolQuery()

      if(thisRawPool && thisRawPool != this.rawPool) {
        this.rawPool = thisRawPool
        this.pool = new Pool(this.rawPool)

        this.drawPool()
      }
    }
  }

  drawPool() {
    let pool = this.pool, templateVars = {}

    this.historyController.addHistory(pool)

    templateVars.address = pool.address

    templateVars.token0 = {
      name: pool.token0.name,
      symbol: pool.token0.symbol,
      icon: pool.tokenIconUrl(pool.token0),
      price: pool.formattedPriceOf(pool.token0)
    }

    templateVars.token1 = {
      name: pool.token1.name,
      symbol: pool.token1.symbol,
      icon: pool.tokenIconUrl(pool.token1),
      price: pool.formattedPriceOf(pool.token1)
    }

    if(this.inverted) {
      const t0 = templateVars.token0

      templateVars.token0 = templateVars.token1
      templateVars.token1 = t0

      templateVars.inverted = true
    }

    templateVars.volume24 = pool.formattedVolume
    templateVars.tvl = pool.formattedTVL
    templateVars.fee = pool.formattedFee

    this.element.innerHTML = render(
      poolTemplate,
      templateVars
    )

    this.chart = new LiquidityChart(this.liquidityChartTarget, this.pool)
    this.chart.drawChart(this.inverted)
  }

  resetChartZoom() {
    this.chart.resetZoom();
  }

  async poolQuery() {
    let _poolQuery = poolQuery

    if(window.selectedDex.startsWith('9inch')) {
      _poolQuery = _poolQuery.replaceAll('derivedETH', 'derivedPLS')
    }

    const response = await fetch(DEXES[window.selectedDex], {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        query: _poolQuery,
        variables: {
          id: this.addressValue.toLowerCase()
        }
      })
    })

    if(!response.ok) {
      return false
    }

    const json = await response.json()

    return json.data.pool
  }

  get poolTemplate() {
    return fs.readFileSync(__dirname + '/../../partials/pool.pug', 'utf8');
  }

  invertPair(event) {
    event.preventDefault()

    this.inverted = this.inverted ? false : true

    this.drawPool()
  }

  get historyController() {
    return this.application.getControllerForElementAndIdentifier(document.querySelector('.history'), 'history');
  }
}

