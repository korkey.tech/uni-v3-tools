import Chart from 'chart.js/auto'
import { getRelativePosition } from 'chart.js/helpers';
import zoomPlugin from 'chartjs-plugin-zoom';

import { Tooltip } from 'chart.js'

import { fetchTicksSurroundingPrice } from './ticks'
import { displayPositions } from './display'

import { render } from 'pug'
import tooltipTemplate from 'string:../partials/tooltip.pug'

Tooltip.positioners.cursor = function(chartElements, coordinates) {
  return coordinates;
}

Chart.register(zoomPlugin)

export default class {

  pool = null
  target = undefined
  ticks = []
  ticksForChart = []

  chart = null

  drawTooltip(context) {
    const tooltipEl = document.querySelector('#liquidity-tooltip'),
          index = context.chart.scales.x.getValueForPixel(
            context.tooltip._eventPosition.x
          ),
          tick = this.ticksForChart[index]

    if(tick) {
      let templateVars = {
        token0: this.pool.token0,
        token1: this.pool.token1,
        price0: this.pool.condensedNumberFormat.format(tick.price0),
        price1: this.pool.condensedNumberFormat.format(tick.price1),
        priceInUSD0:
          this.pool.condensedNumberFormat.format(
            tick.price0 * this.pool.priceOf(this.pool.token1)
          ),
        priceInUSD1:
          this.pool.condensedNumberFormat.format(
            tick.price1 * this.pool.priceOf(this.pool.token0)
          )
      }

      if(tick.price0 < this.pool.priceOf(this.pool.token0)) {
        templateVars.lockedToken = templateVars.token1
        templateVars.lockedLiq = this.pool.condensedNumberFormat.format(tick.tvlToken1)
      } else {
        templateVars.lockedToken = templateVars.token0
        templateVars.lockedLiq = this.pool.condensedNumberFormat.format(tick.tvlToken0)
      }

      tooltipEl.innerHTML = render(tooltipTemplate, templateVars)

      const {offsetLeft: positionX, offsetTop: positionY} = context.chart.canvas

      if(context.tooltip._eventPosition.x + tooltipEl.offsetWidth > screen.width - 20) {
        tooltipEl.style.left = context.tooltip._eventPosition.x - tooltipEl.offsetWidth + 'px';
      } else {
        tooltipEl.style.left = positionX + context.tooltip._eventPosition.x + 'px';
      }

      tooltipEl.style.top = positionY + context.tooltip._eventPosition.y + 'px';
    }
  }

  drawLiquidityProviders(event) {
    const canvasPosition = getRelativePosition(event, this.chart),
          index = this.chart.scales.x.getValueForPixel(canvasPosition.x),
          tick = this.ticksForChart[index]

    displayPositions(this.pool.address, tick.tickIdx)
  }

  constructor(target, pool) {
    this.pool = pool
    this.target = target
  }

  async drawChart(invert=false) {
    this.ticks = await fetchTicksSurroundingPrice(this.pool)
    this.ticksForChart = this.ticks.map((tick) => {
      if(invert) {
        tick.xAxis = this.pool.numberFormat.format(tick.price1)
      } else {
        tick.xAxis = this.pool.numberFormat.format(tick.price0)
      }

      tick.yAxis = tick.activeLiquidity

      if(tick.activeLiquidity > 0) {
        return tick
      } else {
        return undefined
      }

    }).filter(tick => tick)

    this.chart = new Chart(this.target.getContext('2d'), {
      type: 'bar',
      responsive: true,
      maintainAspectRatio: false,
      data: {
        labels: this.ticksForChart.map(tick => {
          return this.pool.numberFormat.format(invert ? tick.price1 : tick.price0)
        }),
        datasets: [{
          data: this.ticksForChart,
          backgroundColor: this.ticksForChart.map(tick => {
            return tick.isCurrent ? 'rgb(255,0,0)' : 'rgb(255,255,255)'
          }),
          borderSkipped: 'start',
          borderRadius: 7,
          hoverBackgroundColor: this.ticksForChart.map(tick => {
            return tick.isCurrent ? 'rgb(160,0,0)' : 'rgb(160,160,160)'
          })
        }]
      },
      interactions: {
        intersect: false,
        mode: 'nearest',
        axis: 'y'
      },
      options: {
        scales:{
          y: {
            display: false
          }
        },
        parsing: {
          xAxisKey: 'xAxis',
          yAxisKey: 'yAxis'
        },
        plugins: {
          legend: {
            display: false
          },
          zoom: {
            zoom: {
              mode: 'x',
              pan: {
                enabled: true,
                mode: 'x'
              },
              drag: {
                enabled: true
              },
              pinch: {
                enabled: true
              }
            }
          },
          tooltip: {
            enabled: false,
            position: 'cursor',
            external: this.drawTooltip.bind(this)
          }
        },
        onClick: this.drawLiquidityProviders.bind(this),
        onHover: function (e, legendItem) {
          e.native.target.style.cursor = "pointer";
        },
        onLeave: function (e) {
          e.native.target.style.cursor = "default";
        }
      }
    })
  }

  resetZoom() {
    this.chart.resetZoom()
  }

}
