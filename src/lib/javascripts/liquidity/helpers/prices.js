import { DEXES } from '../../constants'

let ETHPRICE

if(ETHPRICE == undefined) {

  fetch(DEXES[window.selectedDex], {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      query: `{ bundles(first: 1, subgraphError: allow) {
        ethPriceUSD
      } }`
    })
  }).then(response => {
    if(!response.ok) {
      return null
    }

    return response.json()
  }).then(result => {
    ETHPRICE = result.data.bundles[0].ethPriceUSD
  })

}

export { ETHPRICE }
