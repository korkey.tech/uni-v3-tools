import { ETHPRICE } from './prices'
import { STABLECOINS, ICON_URLS } from '../../constants'

import { getAddress } from '@ethersproject/address'

import { Pool, TICK_SPACINGS } from './9mm.mjs'
import { Token } from '@uniswap/sdk-core'

export default class {

  currencyFormat = new Intl.NumberFormat('en-US', {
    style: 'currency',
    currency: 'USD',
    notation: 'compact',
    maximumFractionDigits: 6,
    maximumSignificantDigits: 3
  })

  numberFormat = new Intl.NumberFormat('en-US', {
    maximumFractionDigits: 6,
    maximumSignificantDigits: 8
  })

  condensedNumberFormat = new Intl.NumberFormat('en-US', {
    notation: 'compact',
    maximumFractionDigits: 6,
    maximumSignificantDigits: 3
  })

  token0 = null
  token1 = null
  poolKlass = null

  constructor(rawPool) {
    this.poolKlass = new Pool(
      new Token(
        1,
        rawPool.token0.address,
        parseInt(rawPool.token0.decimals),
        rawPool.token0.symbol,
        rawPool.token0.name
      ),
      new Token(
        1,
        rawPool.token1.address,
        parseInt(rawPool.token1.decimals),
        rawPool.token1.symbol,
        rawPool.token1.name
      ),
      parseInt(rawPool.feeTier),
      parseInt(rawPool.sqrtPrice),
      parseInt(rawPool.liquidity),
      parseInt(rawPool.tick)
    )

    this.token0 = this.poolKlass.token0
    this.token1 = this.poolKlass.token1

    this.token0.derivedETH = parseFloat(rawPool.token0.derivedETH || rawPool.token0.derivedPLS)
    this.token1.derivedETH = parseFloat(rawPool.token1.derivedETH || rawPool.token1.derivedPLS)

    // Expose the rawPool attrs
    Object.keys(rawPool).forEach(attr => {
      if(attr != 'token0' && attr != 'token1') {
        Object.defineProperty(this, attr, { get() { return rawPool[attr]; } } )
      }
    })

    // Hotifx: Subtract fees from TVL to correct data while subgraph is fixed.
    /**
     * Note: see issue desribed here https://github.com/Uniswap/v3-subgraph/issues/74
     * During subgraph deploy switch this month we lost logic to fix this accounting.
     * Grafted sync pending fix now.
     */
    this.trueTVL0 =
      parseFloat(this.totalValueLockedToken0) - (
        parseFloat(this.volumeToken0) *
        ( parseFloat(this.feeTier) / 10000 / 100 )
      ) / 2

    this.trueTVL1 =
      parseFloat(this.totalValueLockedToken1) - (
        parseFloat(this.volumeToken1) *
        ( parseFloat(this.feeTier) / 10000 / 100 )
      ) / 2
  }

  get tickSpacing() {
    return TICK_SPACINGS[this.feeTier]
  }

  get activeTick() {
    return Math.floor(parseInt(this.tick) / this.tickSpacing) * this.tickSpacing
  }

  get feePercent() {
    return parseFloat(this.feeTier) / 10000 / 100
  }

  get formattedFee() {
    return `${this.feeTier / 10 ** 4}%`
  }

  get formattedVolume() {
    let volumeToken0USD =
          this.priceOf(this.token0) * this.poolDayData[0].volumeToken0,
        volumeToken1USD =
          this.priceOf(this.token1) * this.poolDayData[0].volumeToken1

    return this.currencyFormat.format(volumeToken0USD + volumeToken1USD)
  }

  get tvlUSD() {
    return (
      (
        this.token0.derivedETH > 0 ?
        this.priceOf(this.token0) * this.trueTVL0 :
        0
      ) +
      (
        this.token1.derivedETH > 0 ?
        this.priceOf(this.token1) * this.trueTVL1 :
        0
      )
    )
  }

  get formattedTVL() {
    return this.currencyFormat.format(this.tvlUSD)
  }

  formattedPriceOf(token) {
    return this.currencyFormat.format(this.priceOf(token))
  }

  priceOf(token) {
    if(token.symbol == this.token0.symbol) {
      return this.priceOfToken0
    } else if(token.symbol == this.token1.symbol) {
      return this.priceOfToken1
    }
  }

  get priceOfToken0() {
    // If derived eth is avail. &&
    // (It's not a stable || is a stable and other token has no derived eth)
    if(
      this.token0.derivedETH > 0 && (
        !this.isStableCoin(this.token0) ||
        (this.isStableCoin(this.token0) && this.token1.derivedETH <= 0.000001)
      )
    ) {
      return this.token0.derivedETH * ETHPRICE
    } else {
      return parseFloat(this.poolKlass.priceOf(this.token0).toFixed(20))
    }
  }

  get priceOfToken1() {
    // If derived eth is avail. &&
    // (It's not a stable || is a stable and other token has no derived eth)
    if(
      this.token1.derivedETH > 0 && (
        !this.isStableCoin(this.token1) ||
        (this.isStableCoin(this.token1) && this.token0.derivedETH <= 0.000001)
      )
    ) {
      return this.token1.derivedETH * ETHPRICE
    } else {
      return parseFloat(this.poolKlass.priceOf(this.token1).toFixed(20))
    }
  }

  isStableCoin(token) {
    return STABLECOINS.includes(getAddress(token.address))
  }

  tokenIconUrl(token) {
    return ICON_URLS[window.selectedDex]
           .replace('{addy}', token.address)
           .replace('{addyVer}', getAddress(token.address))
           .replace('{sym}', token.symbol.toLowerCase())
  }

}
