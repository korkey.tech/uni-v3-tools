import { CurrencyAmount } from '@pancakeswap/sdk'
import { BigNumber } from '@ethersproject/bignumber'
import surroundingTicksQuery from "../graphql/surrounding-ticks.graphql"
import JSBI from 'jsbi'
import keyBy from './keyby'
import { DEXES } from '../../constants'

import {
  Pool as NineMMPool,
  TickMath as NineMMTickMath,
  tickToPrice as NineMMtickToPrice,
  TICK_SPACINGS as NineMMTICK_SPACINGS
} from './9mm.mjs'

import {
  Pool as UniPool,
  TickMath as UniTickMath,
  tickToPrice as UnitickToPrice,
  TICK_SPACINGS as UniTICK_SPACINGS
} from '@uniswap/v3-sdk'

let Pool, TickMath, tickToPrice, TICK_SPACINGS

// if(window.selectedDex != 'uni-v3') {
  Pool = NineMMPool
  TickMath = NineMMTickMath
  tickToPrice = NineMMtickToPrice
  TICK_SPACINGS = NineMMTICK_SPACINGS
// } else {
//   Pool = UniPool
//   TickMath = UniTickMath
//   tickToPrice = UnitickToPrice
//   TICK_SPACINGS = UniTICK_SPACINGS
// }

const numSurroundingTicks = 300
const PRICE_FIXED_DIGITS = 20
const MAX_UINT128 = BigNumber.from(2).pow(128).sub(1)

export async function fetchTicksSurroundingPrice(pool) {
  const tickIdxLowerBound = pool.activeTick - numSurroundingTicks * pool.tickSpacing
  const tickIdxUpperBound = pool.activeTick + numSurroundingTicks * pool.tickSpacing

  const ticksResult = await fetchTicks(pool.address, tickIdxLowerBound, tickIdxUpperBound)

  let activeTickIdxForPrice = pool.activeTick

  if (activeTickIdxForPrice < TickMath.MIN_TICK) {
    activeTickIdxForPrice = TickMath.MIN_TICK
  }
  if (activeTickIdxForPrice > TickMath.MAX_TICK) {
    activeTickIdxForPrice = TickMath.MAX_TICK
  }

  let activeTickProcessed = {
    isCurrent: true,
    liquidityActive: JSBI.BigInt(pool.liquidity),
    tickIdx: pool.activeTick,
    liquidityNet: JSBI.BigInt(0),
    price0: tickToPrice(
      pool.token0,
      pool.token1,
      activeTickIdxForPrice
    ).toFixed(PRICE_FIXED_DIGITS),
    price1: tickToPrice(
      pool.token1,
      pool.token0,
      activeTickIdxForPrice
    ).toFixed(PRICE_FIXED_DIGITS),
    liquidityGross: JSBI.BigInt(0)
  }

  const activeTick = ticksResult[pool.activeTick]
  if (activeTick) {
    activeTickProcessed.liquidityGross = JSBI.BigInt(activeTick.liquidityGross)
    activeTickProcessed.liquidityNet = JSBI.BigInt(activeTick.liquidityNet)
  }

  const previousTicks = processSurroundingTicks(pool, activeTickProcessed, ticksResult, 'desc')
  const nextTicks = processSurroundingTicks(pool, activeTickProcessed, ticksResult, 'asc')

  const processedTicks = previousTicks.concat(activeTickProcessed).concat(nextTicks)

  const newData = await Promise.all(
    processedTicks.map(async (t, i) => {
      const active = t.tickIdx === pool.activeTick
      const sqrtPriceX96 = TickMath.getSqrtRatioAtTick(t.tickIdx)
      const mockTicks = [
        {
          index: t.tickIdx - pool.tickSpacing,
          liquidityGross: t.liquidityGross,
          liquidityNet: JSBI.multiply(t.liquidityNet, JSBI.BigInt('-1')),
        },
        {
          index: t.tickIdx,
          liquidityGross: t.liquidityGross,
          liquidityNet: t.liquidityNet,
        },
      ]
      const tmpPool =
        pool.token0 && pool.token1 && pool.feeTier
          ? new Pool(pool.token0, pool.token1, parseInt(pool.feeTier), sqrtPriceX96, t.liquidityActive, t.tickIdx, mockTicks)
          : undefined
      const nextSqrtX96 = processedTicks[i - 1]
        ? TickMath.getSqrtRatioAtTick(processedTicks[i - 1].tickIdx)
        : undefined
      const maxAmountToken0 = pool.token0 ? CurrencyAmount.fromRawAmount(pool.token0, MAX_UINT128.toString()) : undefined

      let outputRes0 = null
      if(tmpPool && maxAmountToken0) {
        outputRes0 = await tmpPool.getOutputAmount(maxAmountToken0, nextSqrtX96)
      } else {
        outputRes0 = []
      }

      const token1Amount = outputRes0[0]

      const amount0 = token1Amount ? parseFloat(token1Amount.toExact()) * parseFloat(t.price1) : 0
      const amount1 = token1Amount ? parseFloat(token1Amount.toExact()) : 0

      return {
        index: i,
        tickIdx: t.tickIdx,
        isCurrent: active,
        activeLiquidity: parseFloat(t.liquidityActive.toString()),
        price0: parseFloat(t.price0),
        price1: parseFloat(t.price1),
        tvlToken0: amount0,
        tvlToken1: amount1,
      }
    })
  )
  // offset the values to line off bars with TVL used to swap across bar
  newData?.map((entry, i) => {
    if (i > 0) {
      newData[i - 1].tvlToken0 = entry.tvlToken0
      newData[i - 1].tvlToken1 = entry.tvlToken1
    }
  })

  return newData
}

async function fetchTicks(poolAddress, tickIdxLowerBound, tickIdxUpperBound) {
  let ticks = [], ticksResult = [], skip = 0

  do {
    const response = await fetch(DEXES[window.selectedDex], {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        query: surroundingTicksQuery,
        variables: {
          poolAddress: poolAddress,
          tickIdxUpperBound: tickIdxUpperBound,
          tickIdxLowerBound: tickIdxLowerBound,
          skip: skip
        }
      })
    })

    if(!response.ok) {
      return false
    }

    const json = await response.json()

    ticks = json.data.ticks
    ticksResult = ticksResult.concat(ticks)
    skip += 1000
  } while (ticks.length > 0)

  return keyBy(ticksResult, 'tickIdx')
}

function processSurroundingTicks(pool, activeTickProcessed, ticksResult, direction) {
  let previousTickProcessed = {
    ...activeTickProcessed
  }

  // Iterate outwards (either up or down depending on 'Direction') from the active tick,
  // building active liquidity for every tick.
  let processedTicks = []
  for (let i = 0; i < numSurroundingTicks; i++) {
    const currentTickIdx =
      direction == 'asc'
        ? previousTickProcessed.tickIdx + pool.tickSpacing
        : previousTickProcessed.tickIdx - pool.tickSpacing

    if (currentTickIdx < TickMath.MIN_TICK || currentTickIdx > TickMath.MAX_TICK) {
      break
    }

    let currentTickProcessed = {
      liquidityActive: previousTickProcessed.liquidityActive,
      tickIdx: currentTickIdx,
      liquidityNet: JSBI.BigInt(0),
      price0: tickToPrice(
        pool.token0,
        pool.token1,
        currentTickIdx
      ).toFixed(PRICE_FIXED_DIGITS),
      price1: tickToPrice(
        pool.token1,
        pool.token0,
        currentTickIdx
      ).toFixed(PRICE_FIXED_DIGITS),
      liquidityGross: JSBI.BigInt(0),
    }

    // Check if there is an initialized tick at our current tick.
    // If so copy the gross and net liquidity from the initialized tick.
    let currentInitializedTick = ticksResult[currentTickIdx.toString()]
    if (currentInitializedTick) {
      currentTickProcessed.liquidityGross = JSBI.BigInt(currentInitializedTick.liquidityGross)
      currentTickProcessed.liquidityNet = JSBI.BigInt(currentInitializedTick.liquidityNet)
    }

    // Update the active liquidity.
    // If we are iterating ascending and we found an initialized tick we immediately apply
    // it to the current processed tick we are building.
    // If we are iterating descending, we don't want to apply the net liquidity until the following tick.
    if (direction == 'asc' && currentInitializedTick) {
      currentTickProcessed.liquidityActive = JSBI.add(
        previousTickProcessed.liquidityActive,
        JSBI.BigInt(currentInitializedTick.liquidityNet)
      )
    } else if (direction == 'desc' && JSBI.notEqual(previousTickProcessed.liquidityNet, JSBI.BigInt(0))) {
      // We are iterating descending, so look at the previous tick and apply any net liquidity.
      currentTickProcessed.liquidityActive = JSBI.subtract(
        previousTickProcessed.liquidityActive,
        previousTickProcessed.liquidityNet
      )
    }

    processedTicks.push(currentTickProcessed)
    previousTickProcessed = currentTickProcessed
  }

  if (direction == 'desc') {
    processedTicks = processedTicks.reverse()
  }

  return processedTicks
}
