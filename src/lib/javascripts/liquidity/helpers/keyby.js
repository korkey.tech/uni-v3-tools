export default function keyBy(array, key) {
  return (array || []).reduce((r, x) => ({ ...r, [key ? x[key] : x]: x }), {})
}
