import { render } from 'pug'

export function displayPool(poolAddress) {
  window.history.pushState({}, "", `#${poolAddress}`);

  document.querySelector('#positions-viewer').innerHTML = ''
  document.querySelector('#pool-viewer').innerHTML = render(
    `#pool.pool(
      data-controller="pool"
      data-pool-address-value="${poolAddress}"
    )`
  )
}

export function displayPositions(poolAddress, tickIdx) {
  const tickId = `${poolAddress}:${tickIdx}`

  window.history.replaceState({}, "", `#${tickId}`);

  document.querySelector('#positions-viewer').innerHTML = render(
    `.positions(
      id="${tickId}"
      data-controller="positions"
      data-positions-pool-address-value="${poolAddress}"
      data-positions-tick-value="${tickIdx}"
    )`
  )
}
