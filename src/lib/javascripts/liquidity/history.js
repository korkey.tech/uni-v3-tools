import { Controller } from "@hotwired/stimulus"
import { getAddress } from '@ethersproject/address'
import { render } from 'pug'

export default class extends Controller {
  static targets = [
    'list'
  ]

  localStorageKey = 'liquidity-history'

  connect() {
    this.writeHistoryList()
  }

  toggle(event) {
    document.body.classList.toggle('history-toggled')
  }

  addHistory(pool) {
    const newItem = {
      address: getAddress(pool.address),
      feeTier: pool.formattedFee,
      symbol: `${pool.token0.symbol}/${pool.token1.symbol}`,
      datetime: new Date()
    }

    // If this historyItem is here, remove it
    const historyArray = (this.history || []).filter(item => {
      return item.address != newItem.address
    })

    historyArray.unshift(newItem)
    this.writeHistory(historyArray)
  }

  removeHistory(pool) {
    const historyArray = (this.history || []).filter(item => {
      return item.address != pool.address
    })

    this.writeHistory(historyArray)
  }

  writeHistory(newHistory) {
    localStorage.setItem(this.localStorageKey, JSON.stringify(newHistory.slice(0,10)))
    this.writeHistoryList()
  }

  writeHistoryList() {
    this.listTarget.innerHTML = ''

    if(this.history.length == 0) {
      this.listTarget.innerHTML = render('div ~ No History Yet ~')
    } else {
      this.history.forEach(item => {
        this.listTarget.insertAdjacentHTML('beforeend', this.historyListItem(item))
      })
    }
  }

  historyListItem(listItem) {
    let historyDate = new Date(listItem.datetime)
    let url = window.location.origin + window.location.pathname + '#' + listItem.address

    return render(`li
      a(data-action="history#toggle" href="${url}" class="history-item")
        | ${listItem.symbol} (${listItem.feeTier})
        br
        | ${listItem.address}

      time(datetime="${historyDate.toJSON()}") ${historyDate.toLocaleString()}`
    )
  }

  get history() {
    return JSON.parse(localStorage.getItem(this.localStorageKey)) || []
  }


  arraysEqual(a,b) {
    a = Array.isArray(a) ? a : [];
    b = Array.isArray(b) ? b : [];
    return a.length === b.length && a.every((el, ix) => el === b[ix]);
  }
}

