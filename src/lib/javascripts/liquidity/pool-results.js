import { Controller } from "@hotwired/stimulus"
import Pool from './helpers/pool'
import { displayPool } from './helpers/display'
import { render } from 'pug'

import poolResultsTemplate from 'string:./partials/pool-result.pug'

export default class extends Controller {
  showLoading() {
    this.clear()
    this.element.insertAdjacentHTML('afterbegin', render(
      `.pool-loading Loading...`
    ))
  }

  drawPoolResults(poolResults) {
    if(poolResults.length == 0) {
      this.element.insertAdjacentHTML('afterbegin', render(
        `.pool-not-found No Pools Found`
      ))

      return
    }

    this.element.insertAdjacentHTML('afterbegin', render(
      `.pool-info.header
        .label Pools
        .volume24 Volume 24
        .tvl TVL
        .price Price`
      ))

    poolResults.forEach(pool => {
      this.drawPool(pool)
    })
  }

  drawPool(rawPool) {
    const pool = new Pool(rawPool)

    let templateVars = {}

    templateVars.address = pool.address
    templateVars.volume24 = pool.formattedVolume
    templateVars.tvl = pool.formattedTVL

    templateVars.token0 = {
      symbol: pool.token0.symbol,
      icon: pool.tokenIconUrl(pool.token0)
    }

    templateVars.token1 = {
      symbol: pool.token1.symbol,
      icon: pool.tokenIconUrl(pool.token1)
    }

    templateVars.fee = pool.formattedFee
    templateVars.price = pool.formattedPriceOf(pool.token0)

    this.element.insertAdjacentHTML('beforeend', render(
      poolResultsTemplate,
      templateVars
    ))
  }

  display({ params: { address }}) {
    this.clear()
    displayPool(address)
  }

  clear() {
    this.element.innerHTML = ''
  }
}

