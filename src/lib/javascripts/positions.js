import * as controllers from './positions/*.js'

for (const [controllerName, controller] of Object.entries(controllers)) {
  Stimulus.register(controllerName, controller.default);
}
