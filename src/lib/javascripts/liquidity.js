import * as controllers from './liquidity/*.js'

for (const [controllerName, controller] of Object.entries(controllers)) {
  Stimulus.register(controllerName, controller.default);
}
